#include "GameState.hpp"
#include "MusicPlayer.hpp"

#include <SFML/Graphics/RenderWindow.hpp>
//Defines the Game state- Run Game
GameState::GameState(StateStack& stack, Context context)
	: State(stack, context)
	, mWorld(*context.window, *context.fonts, *context.sounds, false)
<<<<<<< HEAD
	, mPlayer(nullptr, 1, context.keys1)
{
	mWorld.addAircraft(1);
	mPlayer.setMissionStatus(Player::MissionRunning);
=======
	, mPlayer1(nullptr, 1, context.keys1)
	, mPlayer2(nullptr, 2, context.keys2)
{
	mWorld.addAircraft1(1);
	mWorld.addAircraft2(2);
	mPlayer1.setMissionStatus(Player::MissionRunning);
	mPlayer2.setMissionStatus(Player::MissionRunning);
>>>>>>> 2personWorking

	// Play game theme
	context.music->play(Music::MissionTheme);
}

void GameState::draw()
{
	mWorld.draw();
}

bool GameState::update(sf::Time dt)
{
	mWorld.update(dt);

	if (!mWorld.hasAlivePlayer())
	{
<<<<<<< HEAD
		mPlayer.setMissionStatus(Player::MissionFailure);
=======
		mPlayer1.setMissionStatus(Player::MissionFailure);
		mPlayer2.setMissionStatus(Player::MissionFailure);
>>>>>>> 2personWorking
		requestStackPush(States::GameOver);
	}
	else if (mWorld.hasPlayerReachedEnd())
	{
<<<<<<< HEAD
		mPlayer.setMissionStatus(Player::MissionSuccess);
=======
		mPlayer1.setMissionStatus(Player::MissionSuccess);
		mPlayer2.setMissionStatus(Player::MissionSuccess);
>>>>>>> 2personWorking
		requestStackPush(States::GameOver);
	}

	CommandQueue& commands = mWorld.getCommandQueue();
<<<<<<< HEAD
	mPlayer.handleRealtimeInput(commands);
=======
	mPlayer1.handleRealtimeInput(commands);
	mPlayer2.handleRealtimeInput(commands);
>>>>>>> 2personWorking

	return true;
}

bool GameState::handleEvent(const sf::Event& event)
{
	// Game input handling
	CommandQueue& commands = mWorld.getCommandQueue();
<<<<<<< HEAD
	mPlayer.handleEvent(event, commands);
=======
	mPlayer1.handleEvent(event, commands);
	mPlayer2.handleEvent(event, commands);
>>>>>>> 2personWorking

	// Escape pressed, trigger the pause screen
	if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
		requestStackPush(States::Pause);

	return true;
}
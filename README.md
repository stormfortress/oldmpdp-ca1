# README #



### What is this repository for? ###

Repo for dev work on StormFortress, the floating castle game.
*V0.5 - code currently consists of notated Lecturer code

### How do I get set up? ###

*Download the contents of Repo. Open solution in VS 15 or 17. 
*Download and extract SFML 2.3.2 32bit for VS15. Note it's location.
*Set Includes etc in project properties pointing to SFML folders.
*Debug project to run game

### Contribution guidelines ###

*TBD

### Who do I talk to? ###

*Any team members